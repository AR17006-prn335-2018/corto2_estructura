#include <stdio.h>
#include <math.h>
//Estruct para puntos en x
struct Puntosx{
	
	float x1;
	float x2;
	}puntosx;
//Estruct para puntos en y	
struct Puntosy{
	
	float y1;
	float y2;
	}puntosy;
	
int main(){
	//Ingresamos los puntos en x
	printf("\nIngrese el punto x1\t");
	scanf("%f",&puntosx.x1);
    printf("\nIngrese el punto x2\t");
	scanf("%f",&puntosx.x2);
	//Ingresamos los puntos en y
	printf("\nIngrese el punto y1\t");
	scanf("%f",&puntosy.y1);
    printf("\nIngrese el punto y2\t");
	scanf("%f",&puntosy.y2);
	int opcion;
	//Selecionamos la opcion que deseamos realuzar con los puntos en el plano
	printf("\nSeleccione una opcion \n1.-Calcular Distancia \n2.-Ecuacion de la recta\n3.-salir\n");
	scanf("%d",&opcion);
	//Ejecutamos la opcion selecionada
	do
	{
		//Calculamos la distancia entre los puntos
		if (opcion==1)
		{
		float sx=(puntosx.x2-puntosx.x1)*(puntosx.x2-puntosx.x1);
		float sy=(puntosy.y2-puntosy.y1)*(puntosy.y2-puntosy.y1);
		double sto=sx+sy;
		double resultado=sqrt((sto));
		//Imprimimos la distancia entre los puntos
		printf(" La distancia entrepuntos es %.4f",resultado);
		  }
		//calculamos la ecuacion de la recta
		if (opcion==2)
	  {
		float p1=(puntosy.y2-puntosy.y1)/(puntosx.x2-puntosx.x1);
		//Imprimimos la ecuacion de la recta
		printf("La recta es de la ecuacion es :\n y-%.2f",puntosy.y1);
		printf(" = ");
		printf("%.2f",p1);
		printf("(");
		printf("x -%.2f",puntosx.x1);
		printf(")");
	    	}
		
	//Pedimos nuevamente una opcion para ejecutarla	
	printf("\nSeleccione una opcion \n1.-Calcular Distancia \n2.-Ecuacion de la recta\n3.-salir\n");
	scanf("%d",&opcion);	
	} while(opcion!=3);
	
	return 0;
	}
